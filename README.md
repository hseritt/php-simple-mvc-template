# PHP Simple MVC Template

This project is simply a template for creating simple PHP applications that informally use the MVC paradigm and is very much single-page centric.

For example, let's say we want to create a simple To-Do project.

What we can then do is run:

# ./addpage.sh todo

This will create the following folder structure:

	api/                     Contains folder for each model used.
	api/task                 Folder for task model api's
	api/task/index.php **    index page for model task

	config/                  Holds page and global configuration settings.
	config/settings.php *    Global settings for all pages.
	config/todo.php          Settings that are specific to ToDo sample project.

	content/                 Holds actual content with display formatting.
	content/todo/body.php    The main body page for ToDo project.

	context/                 Contains context information for a page.
	context/todo.php         Specific context for the ToDo page.

	controllers/             Contains controller files
	controllers/todo.php     Specific GET, POST, etc. functions for ToDo app.

	includes/                Holding pages for special designation.           
	includes/page.php *      Any routines that need to be performed for all pages are defined and initialized here.

	lib/                     Holds library packages, files, etc.
	lib/db.php *             Database settings.
	lib/utils.php *          Global functions used by a project.
	lib/todo.php             Specific functions used by ToDo page.

	logs/                    Contains logs. By default app.log is the main log file.

	messages/                Contains strings used by pages. The idea here is to eventually use this also for i18n internationalization string messages.
	messages/todo.php        Contains strings used specifically by ToDo app.

	models/                  Contains functions used for getting data from database.
	models/task.php **       Data functions used specifically ToDo app.

	static/                  Holds static data like CSS, Images and Javascript assets.
	static/bootstrap ***     Bootstrap is distributed here in case needed.
	static/css               CSS files
	static/img               Image files
	static/js                Javascript files

	templates/                       Holds templating/html files for presentation purposes and very little (if any) PHP logic should be used here.
	templates/common *               Common templates used by entire project.
	templates/common/head.php *      Top part of all pages in project.
	templates/common/foot.php *      Bottom part of all pages in project.
	templates/common/submit.php *    Optional submit button for forms.
	templates/forms                  All forms used can go here.
	templates/forms/todo_form.php ** To Do form used for To Do app.
	templates/todo.php               Templates used

	views/                           Holds views page where logic and data are assembled.
	views/todo.php                   View page for ToDo application.

	* Actually is part of the template and not *re-created* when addpage.sh [page] is called.

	** Created manually as part of the ToDo sample project.

	*** Bootstrap files have been placed here getbootstrap.com

## Workflow

  - Add any necessary global settings to config/settings.php
  - Add any settings for the page in config/todo.php
  - Create any tables in your db needed for the page.
  - Create any model functions needed for the page in models/<modelname>.php
  - Create any variables needed in context/todo.php. If helper functions are required, put those in lib/todo.php. Add any necessary global functions in lib/utils.php
  - Create any necessary forms in templates/forms.
  - Create controller GET, POST, etc. functions in controllers/todo.php. Validate any user input there. Create routines to handle cleaned data input and redirect (if necessary) to a landing page.
  - Set up templates/todo.php page to show format as needed. Content should be pulled from .php or .html pages in content/todo/.