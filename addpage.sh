#!/usr/bin/env bash

# Script to create necessary pages in the many directories used.

page=$1

if [ "$page" == "" ]
then
	echo "Usage: ./addpage.sh [pagename]"
	echo "To remove page do: ./addpage.sh [pagename] remove"
	exit
fi

if [ "$2" == "remove" ]
then
 	rm $page.php
 else
	cp .index.php $page.php
fi

dirs=(config context controllers lib messages templates)

for dir in ${dirs[@]}
do
	if [ "$2" == "remove" ]
	then
		rm -rf $dir/$page.php
	else
		cp $dir/.index.php $dir/$page.php
	fi
done

dirs=(api content)

for dir in ${dirs[@]}
do
	if [ "$2" == "remove" ]
	then 
		rm -rf $dir/$page
	else
		mkdir -p $dir/$page
	fi
done

if [ "$2" == "remove" ]
then
	rm -rf static/js/$page.js
else
	touch "static/js/$page.js"
fi