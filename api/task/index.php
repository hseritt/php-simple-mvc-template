<?php

define('BASEDIR', '../..');
include_once('../../config/settings.php');
include_once('../../lib/db.php');
include_once('../../models/tasks.php');

$action = $_GET['action'];
$id = $_GET['id'];
$referer = $_SERVER['HTTP_REFERER'];

switch($action) {
	
	case 'delete':
		delete_task($db, $id);
		break;

	default:
		break;
}

header("Location: ".$referer);

?>