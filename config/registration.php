<?php

# Redirect url after registering
$registered_redirect_url = "/registration.php?registered=true";

# Redirect url when email has been verified
$registration_verified_url = "/registration.php?verified=true";

# Redirect url when email fails verification
$registration_unverified_url = "/registration.php?verified=false";
?>