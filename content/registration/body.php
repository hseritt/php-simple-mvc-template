<h1><?=$registration_header_msg?></h1>

<?php if (!empty($error_list)): ?>
	<ul>
	<?php foreach($error_list as $error): ?>
		<li><?=$error?></li>
	<?php endforeach; ?>
	</ul>
<?php endif; ?>

<form action="" method="POST">
	<?php include_once(APP_FORMS.'/registration.php'); ?>
</form>