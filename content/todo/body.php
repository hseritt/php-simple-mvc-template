<h3><?=$todo_header?></h3>

<p>
	<?=$todo_description?>
</p>

<p>
	<button type="button" onclick="show_todo_form()">
		<?=$todo_new_header?>
	</button>
</p>

<?php if (empty($error_list)): ?>
	<div id="todo-form" class="todo-form" style="display:none">
<?php else: ?>
	<div id="todo-form" class="todo-form" style="display:block">
<?php endif; ?>
	<h3><?=$todo_form_header?></h3>

	<?php if(!empty($error_list)): ?>
		<p><?=$todo_form_err_header?></p>
		<ul>
			<?php foreach($error_list as $error): ?>
				<li><?=$error?></li>
			<?php endforeach; ?>
		</ul>

	<?php endif; ?>

	<form action="" method="POST">
		<table>
			<?php include_once('templates/forms/todo_form.php'); ?>
		</table>
	</form>

</div>

<h3><?=$todo_list_header?></h3>

<table>
<?php foreach($active_tasks as $task): ?>
	<tr>
		<th>Name:</th>
		<td>
			<a href="#task<?=$task['id']?>">
				<?=$task['name']?>
			</a>

			<a href="/api/task/?action=delete&id=<?=$task['id']?>">
				Delete
			</a>
		</td>
	</tr>
	<tr>
		<th>Due:</th>
		<td><?=$task['due']?></td>
	</tr>
	<tr>
		<td>-----------------------------</td>
	</tr>
<?php endforeach; ?>
</table>

<h3><?=$todo_detail_header?></h3>

<?php foreach($active_tasks as $task): ?>

	<h3 id="task<?=$task['id']?>">To-Do Task: <?=$task['name']?></h1>
	<p>
		<a href="/todo.php#">
			Main
		</a>
	</p>

	<?php if (!empty($edit_error_list)): ?>

	<p><?=$todo_detail_form_err_header?></p>
	<ul>
		<?php foreach($edit_error_list as $error): ?>
			<li><?=$error?></li>
		<?php endforeach; ?>
	</ul>

	<?php endif; ?>

	<form id="edit_form<?=$task['id']?>" name="edit_form" action="" method="POST">
		<table>
			<?php include('templates/forms/todo_edit_form.php'); ?>
		</table>
	</form>
<?php endforeach; ?>
