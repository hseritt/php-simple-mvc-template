<?php

// Include any database connections
include_once(DB);

// Include any global lib functions:
include_once(GLOBAL_LIB);

// Include all lib functions for this page
include_once(APP_LIB);

// Include any models files
include_once(MODELS_DIR.'/user.php');

// Assign variables to model functions here for variables needed in views
/*
ob_start();
include('templates/forms/todo_form.php');
$todo_form = ob_get_contents();
ob_end_clean();
*/

$redirect_page = "/login.php";
?>