<?php

// Put all controller logic for GET, POST, etc. requests here.
$error_list = array();

if (isset($_POST['login'])) {
	//Get variables from form
	$username = $_POST['username'];
	$password = $_POST['password'];

	// Validate input

	if (empty($username)) {
		array_push($error_list, "You must provide a username.");
		log_msg(
			LOG_FILE, "TRACE", "User did not provide a username.", __FILE__);
	}

	if (empty($password)) {
		array_push($error_list, "You must provide a password.");
		log_msg(
			LOG_FILE, "TRACE", "User did not provide a username.", __FILE__);
	}

	if (empty($error_list)) {

		$user = login_user($db, $username, $password, LOG_FILE);
		print_r($user);

		if (isset($user)) {
			$_SESSION['user'] = $user;
			header("Location: " . $redirect_page);
		} else {
			array_push($error_list, "Unable to authenticate user.");
		}
	}
}

?>