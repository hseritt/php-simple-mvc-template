<?php

// Put all controller logic for GET, POST, etc. requests here.
$error_list = array();

if (isset($_POST['register'])) {
	
	//Get variables from form
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$password_confirm = $_POST['password_confirm'];
	$email = $_POST['email'];

	// Validate input
	if (empty($first_name)) {
		array_push($error_list, $first_name_empty_err_msg);
	}

	if (empty($last_name)) {
		array_push($error_list, $last_name_empty_err_msg);
	}

	if (empty($username)) {
		array_push($error_list, $username_empty_err_msg);
	}

	if (empty($password)) {
		array_push($error_list, $password_empty_err_msg);
	}

	if (empty($password_confirm)) {
		array_push($error_list, $password_confirm_err_msg);
	}

	if ($password !== $password_confirm) {
		array_push($error_list, $password_match_err_msg);
	}

	if ((!preg_match('#[\d]#',$password)) || (!preg_match('/[a-zA-Z]/', $password))) 
	{
		array_push($error_list, $password_contain_err_msg);
	}

	if ((strlen($password) < 8) || (strlen($password) > 20)) {
		array_push($error_list, $password_len_err_msg);
	}

	if (empty($email)) {
		array_push($error_list, $email_empty_err_msg);
	}

	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		array_push($error_list, $email_format_err_msg);
	}

	if (username_exists($db, $username)) {
		array_push($error_list, $username_exists_err_msg);
	}

	if (email_exists($db, $email)) {
		array_push($error_list, $email_exists_err_msg);
	}

	if (empty($error_list)) {
		// From context file included, add any actions that need to be done with input
		include_once('models/user.php');

		$password = password_hash($password, PASSWORD_DEFAULT);
		$active = 0;
		$verified = 0;
		$verification_key = generate_verification_key();
		$created = date("Y-m-d H:i:s");

		create_user($db, $username, $password, $email, $first_name, $last_name, $active, $verified, $verification_key, $created);

		send_verification_email($server_url, $email, $verification_key, $registration_email_subj, $registration_email_msg);
		
		#Redirect to page
		header("Location: $registered_redirect_url");
	}

}

if (isset($_GET['verify_email'])) {
		
	$verification_key = $_GET['verify_email'];
	if (email_verified($db, $verification_key)) {
		header("Location: $registration_verified_url");
	} else {
		header("Location: $registration_unverified_url");
	}
}

?>