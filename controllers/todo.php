<?php

// Put all controller logic for GET, POST, etc. requests here.
$error_list = array();
$edit_error_list = array();

if (isset($_POST['create'])) {

	//Get variables from form
	$name = $_POST['name'];
	$description =$_POST['description'];
	$due = $_POST['due'];

	// Validate input
	if (empty($name)) {
		array_push($error_list, $name_empty_err_msg);
	}

	if (empty($due)) {
		array_push($error_list, $due_empty_err_msg);
	}

	if (empty($error_list)) {
		// From context file included:
		add_task($db, $name, $description, $due);
		// Redirect to page
		header("Location: $todo_redirect_page");
	}
} else if (isset($_POST['save'])) {

	$id = $_POST['task_id'];
	$name = $_POST['name'];
	$description = $_POST['description'];
	$due = $_POST['due'];

	// Validate input
	if (empty($name)) {
		array_push($edit_error_list, $name_empty_err_msg);
	}

	if (empty($due)) {
		array_push($edit_error_list, $due_empty_err_msg);
	}

	if (empty($edit_error_list)) {
		// From context file included:
		update_task($db, $name, $description, $due, $id);
		// Redirect to page
		header("Location: $todo_redirect_page".$id);
	}
}

?>