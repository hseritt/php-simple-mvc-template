<?php

$url = DB_TYPE.":host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME;

$db = new \PDO(
	$url, 
	DB_USER,
	DB_PASS,
	array(
		PDO::ATTR_PERSISTENT => DB_PERSISTENCE
	)
);
?>