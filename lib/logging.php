<?php

function log_msg($logfile, $level, $message, $scriptname) {
	
	$ts = date(LOG_DT_FORMAT);

	$message = $ts . " - [" . $level . "] - [" . $scriptname . "] - " . $message;

	$fd = fopen($logfile, "a");

	fwrite($fd, $message . "\n");
	fclose($fd);
}

?>
