<?php

function send_verification_email($server_url, $email, $verification_key, $registration_email_subj, $registration_email_msg) {
	
	$verification_link = $server_url."/registration.php?verify_email=$verification_key";
	$registration_email_msg .= "\n$verification_link\n";

	$to = $email;
	$subject = $registration_email_subj;
	$message = $registration_email_msg;

	mail($to, $subject, $message);
}

function email_verified($db, $verification_key) {
	$verified = false;
	$stmt = $db->prepare("select id from `user` where verification_key=:verification_key");
	$stmt->bindParam(":verification_key", $verification_key);
	$stmt->execute();
	$users = $stmt->fetchAll();

	foreach ($users as $user) {
		$stmt = $db->prepare("update `user` set active=1, verified=1 where id=:id");
		$id = $user['id'];
		$stmt->bindParam(":id", $id);
		$stmt->execute();
		$verified = true;
	}

	return $verified;
}

function username_exists($db, $username) {
	$username_exists = false;
	$stmt = $db->prepare("select username from `user` where username=:username");
	$stmt->bindParam(":username", $username);
	$stmt->execute();
	foreach($stmt->fetchAll() as $user) {
		$username_exists = true;
	}
	return $username_exists;
}

function email_exists($db, $email) {
	$email_exists = false;
	$stmt = $db->prepare("select email from `user` where email=:email");
	$stmt->bindParam(":email", $email);
	$stmt->execute();
	foreach($stmt->fetchAll() as $user) {
		$email_exists = true;
	}
	return $email_exists;
}

function generate_verification_key() {
	return md5(uniqid(rand(), true));
}

?>