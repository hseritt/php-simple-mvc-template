<?php

// variables for string output goes here:
$registration_email_subj = "Verify your email address";
$registration_email_msg = "Please click on the following link to verify your email: ";
$page_title = "Register for <insert app>";
$first_name_empty_err_msg = "First name cannot be empty.";
$last_name_empty_err_msg = "Last name cannot be empty.";
$username_empty_err_msg = "Username cannot be empty.";
$password_empty_err_msg = "Password cannot be empty.";
$password_confirm_err_msg = "Passwords must be confirmed.";
$password_match_err_msg = "Passwords must match.";
$password_contain_err_msg = "Passwords must contain a number and an alphanumeric character.";
$password_len_err_msg = "Passwords must be at least 8 characters and no more than 20 characters in length.";
$email_empty_err_msg = "Email address cannot be empty";
$email_format_err_msg = "Email address must have proper format.";
$username_exists_err_msg = "Username already exists.";
$email_exists_err_msg = "Email address has already been used.";
$registration_success_msg = "Congratulations. You have successfully registered. Please go check your email for the verification link we sent you.";
$registration_verified_msg = "Your email has been verified.";
$registration_unverified_msg = "Sorry. Unable to verify your email address.";
$registration_header_msg = "Register";
?>