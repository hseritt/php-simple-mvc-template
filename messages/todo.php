<?php

// variables for string output goes here:
$page_title = "To Do List";
$name_empty_err_msg = "Name must not be empty.";
$due_empty_err_msg = "Due time must not be empty.";
$todo_header = "Welcome to the show!";
$todo_description = "This is a very simple To Do task application. It really is not useful for anything except demonstrating this micro-framework.";
$todo_form_header = "Fill in this form to create a To-Do";
$todo_form_err_header = "Please fix the following errors:";
$todo_list_header = "Active To-Do List";
$todo_detail_header = "To-Do Task Detail Pages";
$todo_detail_form_err_header = "Please fix the following errors:";
$todo_new_header = "Create a new task";

?>