<?php

function add_task($db, $name, $description, $due) {

	$due = strtotime($due);
	$due = date("Y-m-d H:i:s", $due);

	$stmt = $db->prepare("insert into `task` (`name`, `description`, `due`, `created`) values (:name, :description, :due, NOW())");
	$stmt->bindParam(":name", $name);
	$stmt->bindParam(":description", $description);
	$stmt->bindParam(":due", $due);
	$stmt->execute();
}

function update_task($db, $name, $description, $due, $id) {

	$due = strtotime($due);
	$due = date("Y-m-d H:i:s", $due);

	$stmt = $db->prepare("update `task` set name=:name, description=:description, due=:due where id=:id");
	$stmt->bindParam(":name", $name);
	$stmt->bindParam(":description", $description);
	$stmt->bindParam(":due", $due);
	$stmt->bindParam(":id", $id);
	$stmt->execute();	
}

function delete_task($db, $id) {

	$stmt = $db->prepare("delete from `task` where id=:id");
	$stmt->bindParam(":id", $id);
	$stmt->execute();
}

function get_active_tasks($db) {
	$stmt = $db->prepare("select * from task where active=1");
	$stmt->execute();
	return $stmt->fetchAll();
}


?>