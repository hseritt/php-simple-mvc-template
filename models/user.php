<?php

function authenticate_user($db, $username, $password, $logfile) {
	
	$authenticated = false;
	
	$stmt = $db->prepare("select username, password from `user` where username=:username");
	$stmt->bindParam(":username", $username);
	$stmt->execute();
	$user = $stmt->fetch();
	
	if (password_verify($password, $user['password'])) {
		$authenticated = true;
	}

	log_msg($logfile, "TRACE", "Authenticated user: ".$username, __FILE__);
	
	return $authenticated;

}

function login_user($db, $username, $password, $logfile) {
	
	$authenticated = authenticate_user($db, $username, $password, $logfile);
	$user = null;
	
	if ($authenticated) {
		
		$stmt = $db->prepare("select id, username, email from `user` where username=:username");
		$stmt->bindParam(":username", $username);
		$stmt->execute();
		$user = $stmt->fetch();
	
	} else {
		log_msg(
			$logfile, "TRACE", "User: $username not authenticated.", __FILE__);
	}
	
	return $user;
}

function create_user($db, $username, $password, $email, $first_name, $last_name, $active, $verified, $verification_key, $created) {

	$stmt = $db->prepare("insert into `user` (username, password, email, first_name, last_name, active, verified, verification_key, created) values (:username, :password, :email, :first_name, :last_name, :active, :verified, :verification_key, :created)");
	$stmt->bindParam(":username", $username);
	$stmt->bindParam(":password", $password);
	$stmt->bindParam(":email", $email);
	$stmt->bindParam(":first_name", $first_name);
	$stmt->bindParam(":last_name", $last_name);
	$stmt->bindParam(":active", $active);
	$stmt->bindParam(":verified", $verified);
	$stmt->bindParam(":verification_key", $verification_key);
	$stmt->bindParam(":created", $created);
	$stmt->execute();

}

?>