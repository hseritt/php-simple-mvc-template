#!/usr/bin/env bash

# For MySQL only

source env.sh

table=$1

if [ "$table" == "" ]
then
	printf "\nBacking up database ...\n"
	mysqldump --no-data -h ${host} -u ${user} -p${password} ${db} > schemas/${db}.sql
else
	printf "\nBacking up table $table ...\n"
	mysqldump --no-data -h ${host} -u ${user} -p${password} ${db} ${table} > schemas/${table}.sql
fi