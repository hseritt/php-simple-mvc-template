#!/usr/bin/env bash

table=$1

./backup.sh $table

./restore.sh $table

printf "\nDone.\n"