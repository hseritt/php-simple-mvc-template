#!/usr/bin/env bash

# MySQL only

source env.sh

table=$1


if [ "$table" == "" ]
then
	printf "\nRestoring database ...\n"
	mysql -h ${host} -u ${user} -p${password} ${db} < schemas/${db}.sql
else
	printf "\nRestoring table to database ...\n"
	mysql -h ${host} -u ${user} -p${password} ${db} < schemas/${table}.sql
fi