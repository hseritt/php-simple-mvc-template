
function show_todo_form() {
	document.getElementById("todo-form").style.display = 'block';
}

function hide_todo_form() {
	document.getElementById("todo-form").style.display = 'none';
}

function submit_edit_task_form(task_id) {
	document.getElementById('edit_form'+task_id).submit();
}
