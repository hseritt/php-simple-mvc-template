<?php

$login_fields = array(
	"username" => array(
		"field_type" => "text",
		"field_name" => "username",
		"label" => "User Name"
	),
	"password" => array(
		"field_type" => "password",
		"field_name" => "password",
		"label" => "Password"
	)
);

?>

<table>
<?php foreach ($login_fields as $field): ?>
	<tr>
		<td><?=$field['label']?></td>
		<td>
			<input type="<?=$field['field_type']?>" name="<?=$field['field_name']?>"/>
		</td>
	</tr>
<?php endforeach; ?>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="login" value="Log In"/>
		</td>
	</tr>
</table>