<?php

$registration_fields = array(
	"first_name" => array(
		"field_type" => "text",
		"field_name" => "first_name",
		"label" => "First name",
		"max_length" => "30"
	),
	"last_name" => array(
		"field_type" => "text",
		"field_name" => "last_name",
		"label" => "Last name",
		"max_length" => "30"
	),
	"email" => array(
		"field_type" => "email",
		"field_name" => "email",
		"label" => "Email address",
		"max_length" => "100"
	),
	"username" => array(
		"field_type" => "text",
		"field_name" => "username",
		"label" => "User Name",
		"max_length" => "20"
	),
	"password" => array(
		"field_type" => "password",
		"field_name" => "password",
		"label" => "Password",
		"max_length" => "20"
	),
	"password_confirm" => array(
		"field_type" => "password",
		"field_name" => "password_confirm",
		"label" => "Confirm Password",
		"max_length" => "20"
	)
);

?>

<table>
<?php foreach ($registration_fields as $field): ?>
	<tr>
		<td><?=$field['label']?></td>
		<td>
			<input type="<?=$field['field_type']?>" name="<?=$field['field_name']?>" maxlength=<?=$field['max_length']?>/>
		</td>
	</tr>
<?php endforeach; ?>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="register" value="Register"/>
		</td>
	</tr>
</table>