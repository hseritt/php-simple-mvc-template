<tr>
	<td>Name:</td>
	<td>
		<input type="text" name="name" value="<?=$task['name']?>" maxlength=50/>
	</td>
</tr>

<tr>
	<td>Description:</td>
	<td>
		<textarea name="description" columns=75 rows=10>
			<?=$task['description']?>
		</textarea>
	</td>
</tr>

<tr>
	<td>Due:</td>
	<td>
		<input type="text" name="due" value="<?=$task['due']?>" maxlength=24/>
		<br>
		*Use datetime format: YYYY-MM-DD HH:MM:SS
		<input type="hidden" name="task_id" value="<?=$task['id']?>"/>
		<input type="hidden" name="save" value="1"/>
	</td>
</tr>

<tr>
	<td></td>
	<td>
		<button type="button" name="save" onclick="submit_edit_task_form(<?=$task['id']?>)">
			Save
		</button>
	</td>
</tr>

<?php #include('templates/common/submit_save.php'); ?>