<?php

include_once('templates/common/head.php');

// Put together the template here only.
// Create content in content/script and include here.
if (isset($_GET['registered']) && ($_GET['registered'])) {
	
	include_once('content/registration/registered.php');

} else if (isset($_GET['verified']) && $_GET['verified'] === "true") {
	
	include_once('content/registration/verified.php');

} else if (isset($_GET['verified']) && $_GET['verified'] === "false") {
	
	include_once('content/registration/unverified.php');

} else {
	
	include_once('content/registration/body.php');

}

include_once('templates/common/foot.php');

?>